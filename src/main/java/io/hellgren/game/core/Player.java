package io.hellgren.game.core;

import io.hellgren.game.Npc;
import io.hellgren.game.Quest;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import lombok.Data;
import org.springframework.cloud.netflix.eureka.EurekaServiceInstance;

@Data
public class Player {

  private String id;
  private EurekaServiceInstance info;
  private List<NpcAcquaintances> npcs = new ArrayList();
  ConcurrentMap<OffsetDateTime, String> points = new ConcurrentHashMap<>();

  public Player(String id, EurekaServiceInstance info, List<Npc> npcs) {
    this.id = id;
    this.info = info;
    npcs.forEach(npc -> this.npcs.add(new NpcAcquaintances(npc)));
  }

  public ConcurrentMap<OffsetDateTime, String> getPoints() {
    return points;
  }

  public void addPoint(String pointId) {
    points.put(OffsetDateTime.now(), pointId);
  }

  public static class NpcAcquaintances {

    String name;
    String description;
    List<QuestLog> questList = new ArrayList<>();
    boolean hasMet;

    public NpcAcquaintances(Npc npc) {
      this.name = npc.getName();
      this.description = npc.getDescription();
      npc.getQuestList().forEach(quest -> questList.add(new QuestLog(quest)));
      this.hasMet = false;
    }

    public boolean hasMet() {
      return hasMet;
    }

    public void setHasMet() {
      this.hasMet = true;
    }

    public String getName() {
      return this.name;
    }

    public String getDescription() {
      return this.description;
    }

    public List<QuestLog> getQuestList() {
      return this.questList;
    }

    public class QuestLog {
      private boolean started;
      private boolean completedAtSomePoint;
      private int timesTested;
      private Quest quest;

      public QuestLog(Quest quest) {
        this.quest = quest;
        this.started = false;
        this.completedAtSomePoint = false;
        this.timesTested = 0;
      }

      public boolean isStarted() {
        return started;
      }

      public void start() {
        this.started = true;
      }

      public boolean wasCompletedAtSomePoint() {
        return completedAtSomePoint;
      }

      public void completed() {
        this.completedAtSomePoint = true;
      }

      public Quest getQuest() {
        return quest;
      }

      public int getTimesTested() {
        return timesTested;
      }

      public synchronized void incrementTimesTested() {
        timesTested++;
      }
    }
  }
}
