package io.hellgren.game.core;

import io.hellgren.game.quest.Campaign;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.netflix.eureka.EurekaServiceInstance;
import org.springframework.scheduling.annotation.Scheduled;

@Slf4j
public class Players {

  private final DiscoveryClient discoveryClient;
  private final Campaign campaign;
  private final List<Player> players;

  public Players(DiscoveryClient discoveryClient, Campaign campaign) {
    this.discoveryClient = discoveryClient;
    this.campaign = campaign;
    players = new ArrayList<>();
  }

  @Scheduled(initialDelay = 5000, fixedRate = 10000)
  public void activateAllPlayers() {
    discoveryClient.getServices().parallelStream()
        .forEach(serviceId -> {
          var serviceInstance = (EurekaServiceInstance) discoveryClient.getInstances(serviceId).stream()
              .findFirst().orElseThrow();
          var playerServiceIds = players.stream()
              .map(player -> player.getInfo().getInstanceInfo().getVIPAddress())
              .collect(Collectors.toList());
          if(!playerServiceIds.contains(serviceId)) {
            activatePlayer(serviceId, serviceInstance);
            log.info("Added player '{}'", serviceId);
          }
        });
  }

  public List<Player> activatePlayer(String id, EurekaServiceInstance serviceInstance) {
    var playerOptional = players.stream()
        .filter(player -> player.getId().equals(id))
        .findFirst();
    if(playerOptional.isEmpty()){
      players.add(new Player(id, serviceInstance, campaign.getListOfNpcs()));
    } else {
      log.info("Player {} already activated", id);
    }
    return players;
  }

  public List<Player> getActivePlayers() {
    return players;
  }

  public List<Player> removePlayer(Player player) {
    players.removeIf(player1 -> player1.getId().equals(player.getId()));
    return players;
  }

  public List<String> getAllAvailablePlayers() {
    return discoveryClient.getServices();
  }

}
