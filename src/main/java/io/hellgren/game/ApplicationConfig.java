package io.hellgren.game;

import io.hellgren.game.core.Players;
import io.hellgren.game.quest.Campaign;
import java.time.Duration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ApplicationConfig {

  @Bean
  Players players(DiscoveryClient discoveryClient, Campaign campaign){
    return new Players(discoveryClient, campaign);
  }

  @Bean
  RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
      return restTemplateBuilder
          .setConnectTimeout(Duration.ofMillis(500))
          .setReadTimeout(Duration.ofSeconds(1))
          .build();
  }

  @Bean
  public WebMvcConfigurer corsConfigurer()
  {
    return new WebMvcConfigurer() {
      @Override
      public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins("*");
      }
    };
  }

  @Bean
  public TaskScheduler taskScheduler() {
    TaskScheduler scheduler = new ThreadPoolTaskScheduler();



    return scheduler;
  }

}
