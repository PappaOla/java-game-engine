package io.hellgren.game.ports;

public interface GameEngineTick {

  void tick();

}
