package io.hellgren.game;

import io.hellgren.game.adapters.MessageSocket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

public class SocketTextHandler extends TextWebSocketHandler {

  @Autowired
  MessageSocket messageSocket;

  @Override
  public void afterConnectionEstablished(WebSocketSession session) throws Exception {
    messageSocket.addWebSocketSession(session);
    super.afterConnectionEstablished(session);
  }

  @Override
  public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
    messageSocket.removeWebSocketSession(session);
    super.afterConnectionClosed(session, status);
  }
}
