package io.hellgren.game.adapters;

import io.hellgren.game.core.Player;
import io.hellgren.game.quest.GameInfo;
import io.hellgren.game.quest.QuestOrchestrator;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GameController {

  private final QuestOrchestrator questOrchestrator;

  @Autowired
  public GameController(QuestOrchestrator questOrchestrator) {

    this.questOrchestrator = questOrchestrator;
  }

  @PostMapping(value = "/game/start")
  public void postStartGame() {
    questOrchestrator.startGame();
  }

  @PostMapping(value = "/game/stop")
  public void postStopGame() {
    questOrchestrator.stopGame();
  }

  @GetMapping(value = "/game", produces = MediaType.APPLICATION_JSON_VALUE)
  public GameInfo getGameInfo() {
    return questOrchestrator.getGameInfo();
  }


}
