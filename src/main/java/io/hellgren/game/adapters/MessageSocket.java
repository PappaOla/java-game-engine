package io.hellgren.game.adapters;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

@Slf4j
@Controller
public class MessageSocket {

  private final ObjectMapper objectMapper;
  private List<WebSocketSession> webSocketSessions;


  @Autowired
  public MessageSocket(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
    this.webSocketSessions= new ArrayList<>();
  }

  public void addWebSocketSession(WebSocketSession webSocketSession) {
    this.webSocketSessions.add(webSocketSession);
  }

  public void removeWebSocketSession(WebSocketSession session) {
    webSocketSessions.removeIf(webSocketSession -> webSocketSession.getId().equals(session.getId()));
  }

  public GameMessage sendMessage(String player, String message) {
    var gameMessage = new GameMessage(player, message);
    var socketMessageString = Map.of(
        "player", player,
        "text", gameMessage.getMessage()
    );
    if(webSocketSessions != null) {
        webSocketSessions.parallelStream()
            .forEach(socket -> sendMessage(socketMessageString, socket));
    }
    return gameMessage;
  }

  private void sendMessage(Map<String, String> socketMessageString, WebSocketSession socket) {
    try {
      socket.sendMessage(new TextMessage(objectMapper.writeValueAsString(socketMessageString)));
    } catch (IOException e) {
      log.info(String.format("unable to sen WebSocket message to '%s' (%s)", socket.getId(), socket.getUri()), e);
    }
  }

  @Data
  @AllArgsConstructor
  private static class GameMessage {
    private String player;
    private String message;
  }
}
