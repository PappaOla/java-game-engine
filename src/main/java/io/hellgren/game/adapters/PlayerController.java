package io.hellgren.game.adapters;

import io.hellgren.game.core.Player;
import io.hellgren.game.core.Players;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PlayerController {

  private final Players players;

  @Autowired
  public PlayerController(Players players) {
    this.players = players;
  }

  @GetMapping(value = "/players", produces = MediaType.APPLICATION_JSON_VALUE)
  public List<String> getAvailablePlayers() {
    return players.getAllAvailablePlayers();
  }

  @GetMapping(value = "/players/active", produces = MediaType.APPLICATION_JSON_VALUE)
  public List<String> getActivePlayers() {
    return players.getActivePlayers().stream()
        .map(Player::getId)
        .collect(Collectors.toList());
  }



}
