package io.hellgren.game.quest;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GameInfo {

  private boolean isRunning;
  private List<PlayerInfo> playerList;

  @Data
  @AllArgsConstructor
  public static class PlayerInfo {
    String name;
    Integer points;
  }
}
