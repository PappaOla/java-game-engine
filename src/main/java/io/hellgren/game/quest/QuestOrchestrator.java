package io.hellgren.game.quest;


import io.hellgren.game.Npc;
import io.hellgren.game.QuestResponse;
import io.hellgren.game.adapters.MessageSocket;
import io.hellgren.game.core.Player;
import io.hellgren.game.core.Player.NpcAcquaintances;
import io.hellgren.game.core.Player.NpcAcquaintances.QuestLog;
import io.hellgren.game.core.Players;
import io.hellgren.game.quest.GameInfo.PlayerInfo;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.clapper.util.classutil.AndClassFilter;
import org.clapper.util.classutil.ClassFinder;
import org.clapper.util.classutil.ClassInfo;
import org.clapper.util.classutil.SubclassClassFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class QuestOrchestrator {

  public static final String API_MESSAGE_PATH = "/api/message";
  public static final int TIMES_TESTED_BEFORE_HINT = 5;
  @Autowired
  RestTemplate restTemplate;

  @Autowired
  private DiscoveryClient discoveryClient;

  @Autowired
  private Campaign campaign;

  @Autowired
  private Players players;

  @Autowired
  private MessageSocket messageSocket;

  private boolean gameRunning = false;

  public void startGame() {
    log.info("Starting Game...");
    gameRunning = true;
  }

  public void stopGame() {
    log.info("Game stopped!");
    gameRunning = false;
  }

  @PostConstruct
  public void  findNpcs() {
    var classFinder = new ClassFinder();
    classFinder.addClassPath();
    var npcFilter = new AndClassFilter(new SubclassClassFilter(Npc.class));

    Collection<ClassInfo> foundClasses = new ArrayList<>();
    var npcs = classFinder.findClasses(foundClasses, npcFilter);
    var npcNames = foundClasses.stream()
        .map(ClassInfo::getClassName)
        .sorted()
        .collect(Collectors.toList());
    log.info("Found {} NPCs ({})", npcs, npcNames);
    for (ClassInfo classInfo : foundClasses) {
      Class<?> cl = null;
      try {
        var aClazz = Class.forName(classInfo.getClassName());
        Constructor<?> constructor = aClazz.getConstructor();
        Npc npc = (Npc) constructor.newInstance();
        campaign.addNpc(npc);
      } catch (
          ClassNotFoundException |
          NoSuchMethodException |
          IllegalAccessException |
          InstantiationException |
          InvocationTargetException e
      ) {
        log.warn(String.format("Could not create class '%s'", classInfo.getClassName()), e);
      }
    }
  }

  @Scheduled(initialDelay = 1000, fixedDelay = 5000)
  public void sendNewQuests() {
    if (!gameRunning) return;

    players.getActivePlayers().parallelStream()
        .forEach(player -> {
          var npcWithQuest = player.getNpcs().stream()
              .filter(npc -> {
                var nextQuest = getCurrentQuestFromNpc(npc);
                return nextQuest.isPresent();
              })
              .findFirst();
          if (npcWithQuest.isPresent()) {
            var npc = npcWithQuest.get();
            if (!npc.hasMet()) {
              var request = new Messenger(String.format("%s: %s", npc.getName(), npc.getDescription()));
              sendMessageToPlayer(player, request);
              npc.setHasMet();
            }
            var nextQuestFromNpc = getCurrentQuestFromNpc(npc);
            if(nextQuestFromNpc.isPresent()) {
              var questLog = nextQuestFromNpc.get();
              if (!questLog.isStarted()) {
                if (!questLog.getQuest().isHidden()) {
                  var request = new Messenger(String.format("%s: %s", npc.getName(), questLog.getQuest().getName()));
                  sendMessageToPlayer(player, request);
                  request = new Messenger(String.format("%s: %s", npc.getName(), questLog.getQuest().getDescription()));
                  sendMessageToPlayer(player, request);
                }
                questLog.start();
              }
            } else {
              log.error("You should never get an npc ({}) that has no quest, check logic [NPC Class: {}]", npc.getName(), npc.getClass());
            }
          } else {
            log.info("Player '{}' have completed all quests", player.getInfo().getInstanceInfo().getVIPAddress());
            var request = new Messenger("All quests completed! \n\tCongratulations, just sit tight and rake in those sweet points... \uD83D\uDCB0 \uD83D\uDCB0 \uD83D\uDCB0");
            sendMessageToPlayer(player, request);
          }
        });
  }

  private void sendMessageToPlayer(Player player, Messenger request) {
    messagePlayer(player, request);
    messageSocket.sendMessage(player.getId(), request.getMessage());
  }

  private void messagePlayer(Player player, Messenger request) {
    try {
      restTemplate.postForLocation(player.getInfo().getUri() + API_MESSAGE_PATH, request);
    } catch (RestClientException e) {
      log.info("Message did not reach player: {}", player.getId());
    }
  }

  @Scheduled(initialDelay = 10000, fixedRate = 30000)
  public void testQuests() {
    if (!gameRunning) return;

    players.getActivePlayers().parallelStream()
        .forEach(player -> {
          player.getNpcs().stream()
              .filter(NpcAcquaintances::hasMet)
              .forEach(npc -> {
                npc.getQuestList().stream()
                    .filter(QuestLog::isStarted)
                    .forEach(questLog -> {
                        QuestResponse questResult = null;
                      try {
                        questResult = questLog.getQuest().test(player.getId(), player.getInfo().getUri().toString());
                      } catch (Exception e) {
                        log.info("Exception thrown by {} - {}: {} [{}: {}]",
                            player.getId(),
                            npc.getName(),
                            questLog.getQuest().getName(),
                            e.getClass(),
                            e.getMessage());
                      } finally {
                        questLog.incrementTimesTested();
                      }
                        if (questResult != null && questResult.isSuccess()) {
                          awardPoint(player, npc.getName(), questLog.getQuest().getName());
                          if (!questLog.wasCompletedAtSomePoint()) {
                            questLog.completed();
                            var request = new Messenger(String.format(
                                "%s: Thank you, you have now completed the quest '%s'",
                                npc.getName(), questLog.getQuest().getName()));
                            sendMessageToPlayer(player, request);
                          }
                        } else {
                          messageSocket.sendMessage(
                              player.getId(),
                              String.format("FAILED - %s: %s", npc.getName(), questLog.getQuest().getName())
                          );
                          if (questLog.wasCompletedAtSomePoint()) {
                            var request = new Messenger(String.format("Previous completed quest '%s' have now failed!\n"
                                    + "\tPlease fix before %s discovers it\n"
                                    + "\tQuest description: %s",
                                questLog.getQuest().getName(),
                                npc.getName(),
                                questLog.getQuest().getDescription()));
                            sendMessageToPlayer(player, request);
                          }
                          if (questResult != null && questResult.getMessage() != null && !questResult.getMessage().isEmpty()) {
                            var request = new Messenger(String.format("%s tells you:\n"
                                    + "\t%s\n"
                                    + "\tregarding quest: %s",
                                npc.getName(),
                                questResult.getMessage(),
                                questLog.getQuest().getName()));
                            sendMessageToPlayer(player, request);
                          }
                          var hints2Receive = hintsToGet(questLog.getTimesTested());

                          var hints = questLog.getQuest().getHints();
                          if (hints != null && !hints.isEmpty()) {
                            var hintList = hints.stream()
                                .limit(hints2Receive)
                                .collect(Collectors.toList());

                            if (!hintList.isEmpty()) {
                              var request = new Messenger(String.format("%s: I'll give some hints...%n\t%s",
                                  npc.getName(),
                                  String.join("\n\t", hintList)));
                              sendMessageToPlayer(player, request);
                            }
                          }
                        }
                    });
              });
          log.info("{}: {} points", player.getId(), player.getPoints().size());
        });
  }

  private void awardPoint(Player player, String npcName, String questName) {
    player.addPoint(String.format("%s: %s", npcName, questName));
    log.info("{} was awarded a (1) point for completing quest '{}' ({})",
        player.getInfo().getInstanceInfo().getVIPAddress(),
        questName,
        npcName);
  }

  private Optional<QuestLog> getCurrentQuestFromNpc(NpcAcquaintances npc) {
    return npc.getQuestList().stream()
        .filter(quest -> !quest.isStarted() || quest.isStarted() && !quest.wasCompletedAtSomePoint())
        .findFirst();
  }
  private Optional<QuestLog> getNextHiddenQuestFromNpc(NpcAcquaintances npc) {
    return npc.getQuestList().stream()
        .filter(quest -> !quest.isStarted() && quest.getQuest().isHidden() || quest.isStarted() && !quest.wasCompletedAtSomePoint())
        .findFirst();
  }

  public int hintsToGet(int timesTested) {
    var remainder = timesTested % TIMES_TESTED_BEFORE_HINT;
    return (timesTested - remainder) / TIMES_TESTED_BEFORE_HINT;
  }

  public GameInfo getGameInfo() {
    return GameInfo.builder()
        .isRunning(gameRunning)
        .playerList(
            players.getActivePlayers().stream()
                .map(p -> new PlayerInfo(p.getId(), p.getPoints().size()))
                .collect(Collectors.toList())
        )
        .build();
  }


}
