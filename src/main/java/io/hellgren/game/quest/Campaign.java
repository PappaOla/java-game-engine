package io.hellgren.game.quest;

import io.hellgren.game.Npc;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class Campaign {

  private final List<Npc> listOfNpcs = new ArrayList<>();

  public void addNpc(Npc npc) {
    listOfNpcs.add(npc);
  }

  public List<Npc> getListOfNpcs() {
    return listOfNpcs;
  }
}
