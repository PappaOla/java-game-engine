package io.hellgren.game.quest;

import lombok.Value;

@Value
public class Messenger {

  private String message;

}
