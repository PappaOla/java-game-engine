package io.hellgren.game.basicquests;

import io.hellgren.game.Npc;
import io.hellgren.game.Quest;
import java.util.List;

public class A02_Linnea implements Npc {

  @Override
  public String getName() {
    return "Linnea";
  }

  @Override
  public String getDescription() {
    return "Hejja! Merchant Services";
  }

  @Override
  public List<Quest> getQuestList() {
    return List.of(
        new QuestTeamOption(),
        new QuestTeamCreatePerson(),
        new QuestTeamCRUDPerson()
    );
  }
}
