package io.hellgren.game.basicquests;

import io.hellgren.game.Quest;
import io.hellgren.game.QuestResponse;
import java.util.List;
import org.springframework.web.client.RestTemplate;

public class QuestHelloEn implements Quest {

  public static final String TEST_PATH = "/api/hello/en";

  public QuestHelloEn() {
  }

  @Override
  public QuestResponse test(String playerID, String baseUrl) throws Exception {
    var restTemplate = new RestTemplate();
    var response = restTemplate.getForEntity(baseUrl + TEST_PATH, String.class);
    if (response.hasBody()) {
      var string = response.getBody();
      if(string != null) {
        var helloPresent = HelloVerifier.isHello(string, HelloVerifier.PATTERN_EN);
        if(helloPresent)
          return new QuestResponse(true, "Hello to you too <3");
      }
    }
    return new QuestResponse(false, "Pls, just say `Hello!` back");
  }

  @Override
  public String getName() {
    return "Hello! (en)";
  }

  @Override
  public String getDescription() {
    return "Say hello to me!";
  }

  @Override
  public List<String> getHints() {
    return List.of(String.format("GET @ '%s'", TEST_PATH), "ISO_639-1", "https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes");
  }

}
