package io.hellgren.game.basicquests;

import io.hellgren.game.Quest;
import io.hellgren.game.QuestResponse;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class QuestOrder2Hard implements Quest {

  private static Map<Integer, String> numberMap;

  public QuestOrder2Hard() {
    numberMap = new LinkedHashMap<>(20);
    numberMap.put(1, "One");
    numberMap.put(2, "Two");
    numberMap.put(3, "three");
    numberMap.put(4, "Four");
    numberMap.put(5, "Five");
    numberMap.put(6, "Six");
    numberMap.put(7, "Seven");
    numberMap.put(8, "eight");
    numberMap.put(9, "nine");
    numberMap.put(10, "ten");
  }

  @Override
  public QuestResponse test(String playerId, String baseUrl) throws Exception {
    Integer[] numberArray = new Integer[10];
    var numberList = Arrays.stream(numberMap.keySet().toArray(numberArray))
        .limit(getRandomInteger(1, 10))
        .collect(Collectors.toList());
    Collections.shuffle(numberList);
    var stringsToSort = numberList.stream()
        .map(i -> numberMap.get(i))
        .collect(Collectors.toList());
    var restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(List.of(MediaType.APPLICATION_JSON));
    headers.setContentType(MediaType.APPLICATION_JSON);
    var httpEntity = new HttpEntity(stringsToSort, headers);
    var response = restTemplate.postForEntity(baseUrl + "/api/orderNumbers", httpEntity, String[].class);
    var sortedList = numberList.stream()
        .sorted()
        .map(i -> numberMap.get(i))
        .map(s -> s.toLowerCase(Locale.ROOT))
        .collect(Collectors.toList());
    var responseList = Arrays.stream(response.getBody())
        .map(s -> s.toLowerCase(Locale.ROOT))
        .collect(Collectors.toList());
    if (responseList.equals(sortedList)) {
      return new QuestResponse(true, "Good job, sorting!");
    }
    return new QuestResponse(false, "Pls sort the list and return");
  }

  /* * returns random integer between minimum and maximum range */
  public static int getRandomInteger(int maximum, int minimum) {
    return ((int) (Math.random() * (maximum - minimum))) + minimum;
  }


  @Override
  public String getName() {
    return "Number ordering";
  }

  @Override
  public String getDescription() {
    return "Order all the numbers sent to you";
  }

  @Override
  public List<String> getHints() {
    return List.of();
  }
}
