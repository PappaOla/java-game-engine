package io.hellgren.game.basicquests;

import io.hellgren.game.Quest;
import io.hellgren.game.QuestResponse;
import java.util.List;
import java.util.regex.Pattern;
import org.springframework.web.client.RestTemplate;

public class QuestHello implements Quest {

  public QuestHello() {
  }

  @Override
  public QuestResponse test(String playerID, String baseUrl) throws Exception{
    var restTemplate = new RestTemplate();
    var response = restTemplate.getForEntity(baseUrl + "/api/hello", String.class);
    if (response.hasBody()) {
      var string = response.getBody();
      if(string != null) {
        var helloPresent = HelloVerifier.isHello(string, HelloVerifier.PATTERN_EN);
        if(helloPresent)
          return new QuestResponse(true, "Hello to you too <3");
      }
    }
    return new QuestResponse(false, "Pls, just say `Hello!` back");
  }

  @Override
  public String getName() {
    return "Hello!";
  }

  @Override
  public String getDescription() {
    return "Say hello to me!";
  }

  @Override
  public List<String> getHints() {
    return List.of();
  }

}
