package io.hellgren.game.basicquests;

import com.sun.net.httpserver.Headers;
import io.hellgren.game.Quest;
import io.hellgren.game.QuestResponse;
import java.net.URI;
import java.util.List;
import java.util.Map;
import org.clapper.util.misc.MultiValueMap;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.RequestEntity.HeadersBuilder;
import org.springframework.web.client.RestClientException;

public class QuestTeamCreatePerson implements Quest {

  public static final String API_PERSON = "/api/person";
  public static final String CERES = "Ceres";
  public static final String OLA = "Ola";
  public static final String EMPLOYEE_ID = "1007";

  public QuestTeamCreatePerson() {
  }

  @Override
  public QuestResponse test(String playerID, String baseUrl) throws Exception{
    var restTemplate = new RestTemplateBuilder().build();

    try {
      var person = new QuestTeamPerson();
      person.setEmployeeId(EMPLOYEE_ID);
      person.setName(OLA);
      person.setTeam(CERES);
      HttpHeaders headers = new HttpHeaders();
      headers.setAccept(List.of(MediaType.APPLICATION_JSON));
      headers.setContentType(MediaType.APPLICATION_JSON);
      var httpEntity = new HttpEntity(person, headers );
      var entity = restTemplate.exchange(baseUrl + API_PERSON, HttpMethod.POST, httpEntity, QuestTeamPerson.class);
      if (entity.getStatusCodeValue() != 201)
        return new QuestResponse(false, "You should the correct HTTP status when creating something");
      var teamPerson = entity.getBody();
      if (
          !teamPerson.getTeam().equals(CERES) ||
              !teamPerson.getName().equals(OLA) ||
              !teamPerson.getEmployeeId().equals(EMPLOYEE_ID)
      )
          return new QuestResponse(false, "Usually when creating a resource you return the created resource");

      return new QuestResponse(true, "Good job!");
    } catch (RestClientException e) {
      return new QuestResponse(false, "An exception was thrown when trying to parse your response. Usually when creating a resource you return the created resource.");
    }
  }

  @Override
  public String getName() {
    return "Create team member";
  }

  @Override
  public String getDescription() {
    return "Create new team member @ " + API_PERSON;
  }

  @Override
  public List<String> getHints() {
    return List.of(
        "Allow POST @ " + API_PERSON,
        "Uncertain about the payload? Log with @Slf4j and log.info()",
        "You can catch the RequestBody with @RequestBody-annotation as parameter in your Controller method"
    );
  }

}
