package io.hellgren.game.basicquests;

import io.hellgren.game.Npc;
import io.hellgren.game.Quest;
import java.util.List;

public class A01_DeckardCain implements Npc {

  @Override
  public String getName() {
    return "Deckard Cain";
  }

  @Override
  public String getDescription() {
    return "...I thank you, friend, for coming to my aid.";
  }

  @Override
  public List<Quest> getQuestList() {
    return List.of(
        new QuestAutocomplete(),
        new QuestHello(),
        new QuestHelloEn(),
        new QuestHelloPt(),
        new QuestHelloSv()
    );
  }
}
