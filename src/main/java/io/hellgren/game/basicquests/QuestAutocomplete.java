package io.hellgren.game.basicquests;

import io.hellgren.game.Quest;
import io.hellgren.game.QuestResponse;
import java.util.List;

public class QuestAutocomplete implements Quest {

  public QuestAutocomplete() {
  }

  @Override
  public QuestResponse test(String playerID, String baseUrl) {
    return new QuestResponse(true, "");
  }

  @Override
  public String getName() {
    return "Stay awhile...";
  }

  @Override
  public String getDescription() {
    return "...and listen...";
  }

  @Override
  public List<String> getHints() {
    return List.of();
  }

}
