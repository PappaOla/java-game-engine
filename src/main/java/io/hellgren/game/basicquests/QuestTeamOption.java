package io.hellgren.game.basicquests;

import io.hellgren.game.Quest;
import io.hellgren.game.QuestResponse;
import java.util.List;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class QuestTeamOption implements Quest {

  public static final String API_PERSON = "/api/person";

  public QuestTeamOption() {
  }

  @Override
  public QuestResponse test(String playerID, String baseUrl) throws Exception{
    var restTemplate = new RestTemplate();
    try {
      var response = restTemplate.optionsForAllow(baseUrl + API_PERSON);
      if (response.contains(HttpMethod.POST)) {
        return new QuestResponse(true, "Nice, I'm allowed to create a person!");
      }
    } catch (RestClientException e) {
      // Swallow, could not make OPTIONS call
    }
    return new QuestResponse(false, "I could not find that POST is an allowed method @ /api/person");
  }

  @Override
  public String getName() {
    return "What are my options?";
  }

  @Override
  public String getDescription() {
    return "Allow OPTIONS call";
  }

  @Override
  public List<String> getHints() {
    return List.of(
        "Allow POST @ " + API_PERSON,
        "Just creating a @RequestMapping for post will tell spring to use OPTION"
    );
  }

}
