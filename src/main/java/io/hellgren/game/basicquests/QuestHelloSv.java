package io.hellgren.game.basicquests;

import io.hellgren.game.Quest;
import io.hellgren.game.QuestResponse;
import java.util.List;
import org.springframework.web.client.RestTemplate;

public class QuestHelloSv implements Quest {

  public static final String TEST_PATH = "/api/hello/sv";

  public QuestHelloSv() {
  }

  @Override
  public QuestResponse test(String playerID, String baseUrl) throws Exception {
    var restTemplate = new RestTemplate();
    var response = restTemplate.getForEntity(baseUrl + TEST_PATH, String.class);
    if (response.hasBody()) {
      var string = response.getBody();
      if(string != null) {
        var helloPresent = HelloVerifier.isHello(string, HelloVerifier.PATTERN_SV);
        if(helloPresent)
          return new QuestResponse(true, "Hej på dig! <3");
      }
    }
    return new QuestResponse(false, "Can you say hello in the requested language?");
  }

  @Override
  public String getName() {
    return "Hello! (sv)";
  }

  @Override
  public String getDescription() {
    return "Say hello from Sweden";
  }

  @Override
  public List<String> getHints() {
    return List.of(String.format("GET @ '%s'", TEST_PATH), "ISO_639-1", "https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes");
  }

}
