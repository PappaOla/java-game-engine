package io.hellgren.game.basicquests;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class QuestTeamPerson {

  private String employeeId;
  private String name;
  private String team;

}
