package io.hellgren.game.basicquests;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.hellgren.game.Quest;
import io.hellgren.game.QuestResponse;
import java.util.List;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

public class QuestTeamCRUDPerson implements Quest {

  public static final String API_PERSON = "/api/person";
  public static final String TEAM_NAME = "Chaos";
  public static final String MAX = "Max";
  public static final String EMPLOYEE_ID = "666";

  public QuestTeamCRUDPerson() {
  }

  @Override
  public QuestResponse test(String playerID, String baseUrl) throws Exception{
    var restTemplate = new RestTemplateBuilder().build();

    // Create
    try {
      var person = new QuestTeamPerson();
      person.setEmployeeId(EMPLOYEE_ID);
      person.setName(MAX);
      person.setTeam(TEAM_NAME);
      HttpHeaders headers = new HttpHeaders();
      headers.setAccept(List.of(MediaType.APPLICATION_JSON));
      headers.setContentType(MediaType.APPLICATION_JSON);
      var httpEntity = new HttpEntity(person, headers);
      var entity = restTemplate.exchange(baseUrl + API_PERSON, HttpMethod.POST, httpEntity, QuestTeamPerson.class);
      if (entity.getStatusCodeValue() != 201)
        return new QuestResponse(false, "You should the correct HTTP status when creating something");
      var teamPerson = entity.getBody();
      if (
          !teamPerson.getTeam().equals(TEAM_NAME) ||
              !teamPerson.getName().equals(MAX) ||
              !teamPerson.getEmployeeId().equals(EMPLOYEE_ID)
      ) {
        return new QuestResponse(false, "Usually when creating a resource you return the created resource");
      }
    } catch (RestClientException e) {
      return new QuestResponse(false, "An exception was thrown when trying to parse your response.");
    }

    // Read
    try {
      HttpHeaders headers = new HttpHeaders();
      headers.setAccept(List.of(MediaType.APPLICATION_JSON));
      headers.setContentType(MediaType.APPLICATION_JSON);
      var httpEntity = new HttpEntity(headers);
      var entity = restTemplate.exchange(baseUrl + API_PERSON + "/" + EMPLOYEE_ID, HttpMethod.GET, httpEntity, String.class);
      if (entity.getStatusCodeValue() != 200)
        return new QuestResponse(false, "You should the correct HTTP status when getting something");
      var teamPerson = new ObjectMapper().readValue(entity.getBody(), QuestTeamPerson.class);
      if (
          !teamPerson.getTeam().equals(TEAM_NAME) ||
              !teamPerson.getName().equals(MAX) ||
              !teamPerson.getEmployeeId().equals(EMPLOYEE_ID)
      ) {
        return new QuestResponse(false, "Wrong resource returned");
      }
    } catch (HttpClientErrorException e) {
      return new QuestResponse(false, String.format("Error, did not receive the status code expected, status code received: %s", e.getStatusCode()));
    } catch (RestClientException e) {
      return new QuestResponse(false, "An exception was thrown when trying to parse your response.");
    }

    // Update
    try {
      var person = new QuestTeamPerson();
      person.setEmployeeId(EMPLOYEE_ID);
      person.setName(MAX);
      var newTeamName = "Ceres";
      person.setTeam(newTeamName);
      HttpHeaders headers = new HttpHeaders();
      headers.setAccept(List.of(MediaType.APPLICATION_JSON));
      headers.setContentType(MediaType.APPLICATION_JSON);
      var httpEntity = new HttpEntity(person, headers);
      var entity = restTemplate.exchange(baseUrl + API_PERSON + "/" + EMPLOYEE_ID, HttpMethod.PUT, httpEntity, QuestTeamPerson.class);
      if (entity.getStatusCodeValue() != 200)
        return new QuestResponse(false, "You should the correct HTTP status when updating something");
      var teamPerson = entity.getBody();
      if (
          !teamPerson.getTeam().equals(newTeamName) ||
              !teamPerson.getName().equals(MAX) ||
              !teamPerson.getEmployeeId().equals(EMPLOYEE_ID)
      ) {
        return new QuestResponse(false, "Usually when updating a resource you return the updated resource");
      }

    } catch (HttpClientErrorException e) {
      return new QuestResponse(false,
          String.format("Error, did not receive the status code expected, status code received: %s", e.getStatusCode()));
    } catch (RestClientException e) {
      return new QuestResponse(false, "An exception was thrown when trying to parse your response. You need to return the resource updated");
    }

    // Read
    try {
      HttpHeaders headers = new HttpHeaders();
      headers.setAccept(List.of(MediaType.APPLICATION_JSON));
      headers.setContentType(MediaType.APPLICATION_JSON);
      var httpEntity = new HttpEntity(headers);
      var entity = restTemplate.exchange(baseUrl + API_PERSON + "/" + EMPLOYEE_ID, HttpMethod.GET, httpEntity, String.class);
      if (entity.getStatusCodeValue() != 200)
        return new QuestResponse(false, "You should the correct HTTP status when getting something");
      var teamPerson = new ObjectMapper().readValue(entity.getBody(), QuestTeamPerson.class);
      if (
          !teamPerson.getTeam().equals("Ceres") ||
              !teamPerson.getName().equals(MAX) ||
              !teamPerson.getEmployeeId().equals(EMPLOYEE_ID)
      ) {
        return new QuestResponse(false, "Wrong resource returned");
      }
    } catch (RestClientException e) {
      return new QuestResponse(false, "An exception was thrown when trying to parse your response.");
    }

    // Delete
    try {
      HttpHeaders headers = new HttpHeaders();
      headers.setAccept(List.of(MediaType.APPLICATION_JSON));
      headers.setContentType(MediaType.APPLICATION_JSON);
      var httpEntity = new HttpEntity(headers);
      var entity = restTemplate.exchange(baseUrl + API_PERSON + "/" + EMPLOYEE_ID, HttpMethod.DELETE, httpEntity, String.class);
      if (entity.getStatusCodeValue() != 200)
        return new QuestResponse(false, "You should the correct HTTP status when deleting something");
      var teamPerson = new ObjectMapper().readValue(entity.getBody(), QuestTeamPerson.class);
      if (
          !teamPerson.getTeam().equals("Ceres") ||
              !teamPerson.getName().equals(MAX) ||
              !teamPerson.getEmployeeId().equals(EMPLOYEE_ID)
      ) {
        return new QuestResponse(false, "Wrong resource returned");
      }
    } catch (HttpClientErrorException e) {
      return new QuestResponse(false,
          String.format("Error, did not receive the status code expected, status code received: %s", e.getStatusCode()));
    } catch (RestClientException e) {
      return new QuestResponse(false, "An exception was thrown when trying to parse your response. You need to return the resource updated");
    }

    // Read - 404
    try {
      HttpHeaders headers = new HttpHeaders();
      headers.setAccept(List.of(MediaType.APPLICATION_JSON));
      headers.setContentType(MediaType.APPLICATION_JSON);
      var httpEntity = new HttpEntity(headers);
      var entity = restTemplate.exchange(baseUrl + API_PERSON + "/" + EMPLOYEE_ID, HttpMethod.GET, httpEntity, Void.class);
    } catch (HttpClientErrorException e) {
      if(e.getStatusCode().value() != 404)
        return new QuestResponse(false,
          String.format("Error, did not receive the status code expected, status code received: %s", e.getStatusCode()));
    } catch (RestClientException e) {
      return new QuestResponse(false, "An exception was thrown when trying to parse your response. You need to return the resource updated");
    }

    return new QuestResponse(true, "Good job!");
  }

  @Override
  public String getName() {
    return "CRUD Employee";
  }

  @Override
  public String getDescription() {
    return "\"Full\" CRUD for: " + API_PERSON;
  }

  @Override
  public List<String> getHints() {
    return List.of(
        "Make sure you have all, CRUD implemented"
    );
  }

}
