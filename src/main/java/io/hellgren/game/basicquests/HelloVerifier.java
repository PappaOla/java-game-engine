package io.hellgren.game.basicquests;

import java.util.regex.Pattern;

public class HelloVerifier {

  private HelloVerifier() {
  }

  public static final String PATTERN_EN = "^(\\W+)?hello(\\W+)?$";
  public static final String PATTERN_SV = "^(\\W+)?hej(\\W+)?$";
  public static final String PATTERN_PT = "^(\\W+)?ol[á|a](\\W+)?$";

  public static boolean isHello(String string, String pattern) {
    return Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(string).find();
  }

}
