package io.hellgren.game.basicquests;

import io.hellgren.game.Npc;
import io.hellgren.game.Quest;
import java.util.List;

public class WizardOfOrdering implements Npc {

  @Override
  public String getName() {
    return "Wizard of Ordering";
  }

  @Override
  public String getDescription() {
    return "Please return things in order";
  }

  @Override
  public List<Quest> getQuestList() {
    return List.of(
        new QuestOrder1(),
        new QuestOrder2Hard()
    );
  }
}
