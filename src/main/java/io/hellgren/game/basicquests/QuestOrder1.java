package io.hellgren.game.basicquests;

import io.hellgren.game.Quest;
import io.hellgren.game.QuestResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class QuestOrder1 implements Quest {

  private static List<String> list = List.of(
      "purple",
      "market",
      "weave",
      "elderly",
      "giant",
      "whimsical",
      "airport",
      "grip",
      "greasy",
      "certain",
      "mysterious",
      "bewildered",
      "renounce",
      "thumb",
      "scabble",
      "fax",
      "wakeful",
      "print",
      "witty",
      "gifted"
  );

  @Override
  public QuestResponse test(String playerId, String baseUrl) throws Exception {
    var stringList = list.stream().collect(Collectors.toList());
    Collections.shuffle(stringList);
    var stringsToSort = stringList.stream()
        .limit(10)
        .collect(Collectors.toList());
    var restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(List.of(MediaType.APPLICATION_JSON));
    headers.setContentType(MediaType.APPLICATION_JSON);
    var httpEntity = new HttpEntity(stringsToSort, headers);
    var response = restTemplate.postForEntity(baseUrl + "/api/orderListOfWords", httpEntity, String[].class);
    var sortedList = stringsToSort.stream()
        .sorted()
        .collect(Collectors.toList());
    var responseList = Arrays.stream(response.getBody())
        .collect(Collectors.toList());
    if (responseList.equals(sortedList)) {
      return new QuestResponse(true, "Good job, sorting!");
    }
    return new QuestResponse(false, "Pls sort the list and return");
  }

  @Override
  public String getName() {
    return "Word order";
  }

  @Override
  public String getDescription() {
    return "Order all the words sent to you";
  }

  @Override
  public List<String> getHints() {
    return List.of();
  }
}
