import { useEffect, useState } from 'react';

const useGameInfo = (hostname) => {
  const [gameInfo, setGameInfo] = useState({});

  useEffect(() => {
    const fetchGameInfo = function () {
      fetch(`http://${hostname}:3000/api/game`).then(async (response) => {
        if (response.ok) {
          const jsonResult = await response.json();
          setGameInfo(jsonResult);
        }
      });
    };

    const intervalId = setInterval(fetchGameInfo, 5000);

    return () => {
      clearInterval(intervalId);
    };
  }, []);

  return gameInfo;
};

export default useGameInfo;
