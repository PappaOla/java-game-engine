import { useEffect, useState } from 'react';

const useGameAction = () => {
  useEffect(() => {
    const startGame = function () {
      fetch('http://localhost:3000/api/game/start', {
        method: 'POST',
      }).then(async (response) => {
        if (!response.ok) {
          console.error('Could not start game!, try again...');
        }
      });
    };
  }, []);
};

export default useGameAction;
