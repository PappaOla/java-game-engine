import './Header.css';
import { Typography } from '@material-ui/core';

function Header() {
  return (
    <Typography
      style={{
        margin: '15px 0 15px 0',
      }}
      color="textSecondary"
      variant="h4"
    >
      Java Game Engine
    </Typography>
  );
}

export default Header;
