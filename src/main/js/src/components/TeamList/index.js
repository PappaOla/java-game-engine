import { Grid } from '@material-ui/core';
import React from 'react';

const TeamList = React.memo(({ playerList }) => {
  const sortedList = playerList?.sort((a, b) => b.points - a.points);

  return (
    <>
      {sortedList?.map(({ name, points }) => (
        <Grid
          key={name}
          container
          style={{
            fontSize: '3rem',
            textTransform: 'capitalize',
          }}
        >
          <Grid item sm={5}>
            {name}
          </Grid>
          <Grid item sm={2} />
          <Grid item sm={5}>
            {points}
          </Grid>
        </Grid>
      ))}
    </>
  );
});

export default TeamList;
