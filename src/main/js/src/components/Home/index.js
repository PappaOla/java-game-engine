import React from 'react';
import { Container, Grid, Button, Typography, Fab } from '@material-ui/core';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';
import useGameInfo from '../../hooks/useGameInfo';
import Header from '../Header';
import TeamList from '../TeamList';
import MessageList from '../MessageList';

const fabStyle = {
  margin: 0,
  top: 'auto',
  right: 20,
  bottom: 20,
  left: 'auto',
  position: 'fixed',
};

const Home = ({ hostname }) => {
  const gameInfo = useGameInfo(hostname);

  const handleClick = () => {
    const action = gameInfo.running ? 'stop' : 'start';
    fetch(`http://${hostname}:3000/api/game/${action}`, {
      method: 'POST',
    }).then(async (response) => {
      if (!response.ok) {
        console.error(`Could not ${action} game!, try again...`);
      }
    });
  };

  return (
    <Container maxWidth="lg">
      <Grid container maxWidth="lg">
        <Grid md={6} sm={12} item direction="column" justifyContent="left">
          <Grid container>
            <Grid item sm={12}>
              <Header />
            </Grid>
            <Grid
              item
              sm={12}
              style={{
                margin: '15px 0 25px 0',
              }}
            >
              <Typography variant="h2">Score Board</Typography>
            </Grid>
            <Grid item sm={12}>
              <TeamList playerList={gameInfo?.playerList} />
            </Grid>
          </Grid>
        </Grid>
        <Grid
          item
          md={6}
          sm={12}
          direction="column"
          alignContent="center"
          style={{
            margin: '25px 0 25px 0',
          }}
        >
          <MessageList hostname={hostname} />
        </Grid>
        <Fab
          color={gameInfo?.running ? 'primary' : 'default'}
          style={fabStyle}
          onClick={handleClick}
          aria-label={gameInfo?.running ? 'Stop Game' : 'Start Game'}
        >
          {gameInfo?.running ? <StopIcon /> : <PlayArrowIcon />}
        </Fab>
      </Grid>
    </Container>
  );
};

export default Home;
