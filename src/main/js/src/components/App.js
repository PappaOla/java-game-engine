import { createTheme } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import './App.css';
import Home from './Home';

const theme = createTheme();

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Home hostname={window.location.hostname} />
    </ThemeProvider>
  );
}

export default App;
