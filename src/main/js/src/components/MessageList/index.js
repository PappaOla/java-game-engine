import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Grid, Typography } from '@material-ui/core';

const MAX_LENGTH = 15;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    marginBottom: '15px',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  message: {
    whiteSpace: 'pre-wrap',
  },
  teamName: {
    textTransform: 'capitalize',
  },
}));

const MessageList = ({ hostname }) => {
  const classes = useStyles();
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    const socket = new WebSocket(`ws://${hostname}:9612/ws`);
    socket.addEventListener('message', ({ data }) => {
      setMessages((oldMessages) => {
        oldMessages = oldMessages.slice(0, MAX_LENGTH - 1);
        return [JSON.parse(data), ...oldMessages];
      });
    });
  }, []);

  return messages.map((message, index) => (
    <Grid item sm={8}>
      <Card key={index} className={classes.root}>
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Typography
              className={classes.teamName}
              component="h5"
              variant="h5"
            >
              {message.player}
            </Typography>
            <Typography
              className={classes.message}
              variant="subtitle1"
              color="textSecondary"
            >
              {message.text}
            </Typography>
          </CardContent>
        </div>
      </Card>
    </Grid>
  ));
};

export default MessageList;
