package io.hellgren.game.basicquests;

import static org.junit.jupiter.api.Assertions.*;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class HelloVerifierTest {

  @Test
  void isHelloEn() {
    var testString = "Hello!";
    Assertions.assertThat(HelloVerifier.isHello(testString, HelloVerifier.PATTERN_EN)).isTrue();
  }

  @Test
  void isHelloSv() {
    var testString = "...Hej...";
    Assertions.assertThat(HelloVerifier.isHello(testString, HelloVerifier.PATTERN_SV)).isTrue();
  }

  @Test
  void isHelloPt() {
    var testString = "Ola!";
    Assertions.assertThat(HelloVerifier.isHello(testString, HelloVerifier.PATTERN_PT)).isTrue();
  }

}
